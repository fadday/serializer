using System;

/* В данных не должно соержаться '{' или '}' */
/* Проверок на входные данные и в коде чуть меньше нуля */

public class Program {
    static void Main() {
        Data data = new Data();

        data.A = "VAL_A";
        data.B = "VAL_B";
        data.C = "VAL_C";
        data.G = "VAL_G";

        data.F = new Check();
        data.F.Foo = "VAL_FOO";

		var serializer = new MySerialize ();

        Console.WriteLine("Создали объект data, где data.A = " + data.A + "; и т.д.");
        Console.WriteLine("Сериализуем и получем...");

        var ser_obj = serializer.Serialize(data);

        Console.WriteLine(ser_obj);

        Console.WriteLine("Проводим десериализацию и...");

        var new_data = (Data)serializer.Deserialize(ser_obj);

        Console.WriteLine("...получаем объект new_data, где new_data.A = " + new_data.A + ";");

        if (data.A == new_data.A) {
            Console.WriteLine("PROFIT!!!");
        } else {
            Console.WriteLine("Что то пошло не так...");
        }
    }
}
