using System;

/* Помечаем свойство для сериализации*/
[System.AttributeUsage(System.AttributeTargets.Property)]
public class MySerializableProperty : Attribute {};

/* Помечаем свойство как объект для сериализации*/
[System.AttributeUsage(System.AttributeTargets.Property)]
public class MySerializableObject : Attribute {};
