using System;
using System.Reflection;

/* Работает только с данными типа String и в них не должно содежаться символов { и } */
/* Вообще по уму стоило бы релизовать IFormatter. */

public class MySerialize {

    /* Сериализуем во что-то  похожее на JSON */
    public string Serialize(Object obj) {
        string result = string.Empty;
		var type = obj.GetType ();

		result += "{\"" + type.Name + "\":{";

        var properties = type.GetProperties();

        var temp_cnt = 0;
		foreach (PropertyInfo property in properties) {
			if (property.GetCustomAttribute<MySerializableProperty> () != null) {
                var sign = (temp_cnt > 0) ? "," : "";
                result += sign;
				result += "\"" + property.Name + "\":";
				result += "\"" + property.GetValue (obj).ToString () + "\"";
			} else if (property.GetCustomAttribute<MySerializableObject> () != null) {
                var sign = (temp_cnt > 0) ? "," : "";
                result += sign;
                result += "\"" + property.Name + "\":";
				result += Serialize (property.GetValue (obj));
			} else {
				continue;
			}

            temp_cnt++;
        }

		return result += "}}";
    }

    public Object Deserialize(string obj) {
        var obj_type = getObjectString(obj).Split(new Char[] {':'}, 2)[0].Trim(new Char[] {'\"'});
        var obj_inst = Activator.CreateInstance(Type.GetType(obj_type));

        return initProperties(getPropertyStrings(getObjectString(getObjectString(obj))), obj_inst);
    }

    private string getObjectString(string obj) {
        var begin_pos = obj.IndexOf('{') + 1;
        var end_pos = obj.LastIndexOf('}');

        if ((begin_pos == -1) || (end_pos == -1)) {
            return string.Empty;
        }

        var length = end_pos - begin_pos;

        return obj.Substring(begin_pos, length);
    }

    private string[] getPropertyStrings(string obj) {
        return obj.Split(',');
    }

    private bool isObject(string str) {
        if (str.IndexOf('{') == 0) { 
            return true;
        } else {
            return false;
        }
    }

    private object initProperties(string[] properties, object init) {
        foreach (var property in properties) {
            var temp_prop = property.Split(new Char[] {':'}, 2);

            var prop_name = temp_prop[0].Trim(new Char[] {'\"'});
            var prop_val  = temp_prop[1].Trim(new Char[] {'\"'});

            if (isObject(prop_val)) {
                var obj_type = getObjectString(prop_val).Split(new Char[] {':'}, 2)[0].Trim(new Char[] {'\"'});
                var obj_inst = Activator.CreateInstance(Type.GetType(obj_type));

                init.GetType().GetProperty(prop_name).SetValue(init, initProperties(getPropertyStrings(getObjectString(getObjectString(property))), obj_inst));
            } else {
                init.GetType().GetProperty(prop_name).SetValue(init, prop_val);
            }
        }

        return init;
    }
}
