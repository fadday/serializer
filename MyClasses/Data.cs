/* Тестовые классы */

public class Check {
    public Check () {
    }

	[MySerializableProperty]
    public string Foo {
        get; set;
    }
}

public class Data {
    public Data () {
    }

	[MySerializableProperty]
    public string A {
        get; set;
    }

    public string B {
        get; set;
    }

	[MySerializableProperty]
    public string C {
        get; set;
    }

	[MySerializableObject]
    public Check F {
        get; set;
    }

    [MySerializableProperty]
    public string G {
        get; set;
    }
}
